using System;

namespace ExampleWebSite.Models
{
    public class HomeModel
    {
        public HomeModel()
        {
            Date1 = DateTime.Now;
            Date2 = DateTime.Now;
            Date3 = DateTime.Now;
            Date4 = DateTime.Now;
        }

        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }
        public DateTime Date3 { get; set; }
        public DateTime Date4 { get; set; }
    }
}