﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Forloop.HtmlHelpers")]
[assembly: AssemblyDescription("HtmlHelpers for managing scripts for Razor Partial Views and Templates in ASP.NET MVC")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("forloop")]
[assembly: AssemblyProduct("Forloop.HtmlHelpers")]
[assembly: AssemblyCopyright(@"Copyright (c) 2013 forloop - Russ Cam http://forloop.co.uk
                               -------------------------------------------------------
                               Dual licensed under the MIT and GPL licenses.
                                 - http://www.opensource.org/licenses/mit-license
                                 - http://www.opensource.org/licenses/gpl-3.0")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("71a173ce-4ce3-4e07-85ae-119964d5b802")]

[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]

[assembly: InternalsVisibleTo("Forloop.UnitTests")]
